<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;
class convertJsonToExcel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convertJson';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'convertJsonToExcel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::disk('local')->files('/imports');
        foreach ($files as $filePath){

            $car_json = base_path('storage/app/'. $filePath);

            $file = file_get_contents($car_json);

            $jsonDecoded = json_decode($file, true);
            $jsonDecoded = $jsonDecoded['results']['data'];

            //Give our CSV file a name.
            $file_name = str_replace("imports/", "", $filePath);
            //check rewrite file
            $check_no_exist = true;
            $pattern = '/_[0-9].txt/';
            if (preg_match($pattern, $file_name)){
                $file_name = preg_replace('/_[0-9].txt/', ".csv", $file_name);
                $check_no_exist = false;
            }
            else
            {
                $file_name = str_replace("txt", "csv", $file_name);
            }

            echo "\n Convert File : ". $file_name;
            $csvFileName = 'storage/app/exports/' . $file_name;

            //Open file pointer.
            $fp = fopen($csvFileName, 'a+');

            $array_key = $jsonDecoded[0];

            $array_key = array_keys($array_key);

            $insert_key = array_slice($array_key, 58, 11, true);
            array_splice($array_key, 87, 1000);
            array_splice($array_key, 58, 11);
            array_splice($array_key, 17, 0, $insert_key);
            $array_key_remove = [];

            //Loop through the associative array.
            foreach ($jsonDecoded as $key => &$row){
                $row = array_values($row);

                $insert = array_slice($row, 58, 11, true);
                array_splice($row, 87, 1000);
                array_splice($row, 58, 11);
                array_splice($row, 17, 0, $insert);

                foreach ($row as $key_1 => &$row_1) {
                    if (is_array($row_1)) {
                        if(!in_array($key_1, $array_key_remove))
                            array_push($array_key_remove, $key_1);
                        unset($row[$key_1]);
                    }
                }
            }

            //unset header
            foreach ($array_key_remove as &$value){
                unset($array_key[$value]);
                // // foreach ($jsonDecoded as $key => &$row_2){
                //     unset($row_2[$value]);
                // // }
            }

            //convert to csv file
            if ($check_no_exist)
                fputcsv($fp, $array_key);
            foreach($jsonDecoded as $key => &$row){
                fputcsv($fp, $row);
            }

            //Finally, close the file pointer.
            fclose($fp);
        }

    }
}
